package FileCopying;
import java.io.*;
import java.util.Scanner;
 class CopyFile {
    final private static String ENTER_THE_PATH_OF_FILE = "Введите полный путь файла, который нужно скопировать(с текстом): ";
    final private static String ENTER_THE_PATH_TO_COPY = "Введите полный путь, куда нужно скопировать копировать файл: ";

      static void fileCopy(){
        Scanner scanner = new Scanner(System.in);
        System.out.print(ENTER_THE_PATH_OF_FILE);
        String textSource = scanner.nextLine();
        System.out.print(ENTER_THE_PATH_TO_COPY);
        String textResult = scanner.nextLine();
         try (BufferedReader input = new BufferedReader(new FileReader(textSource));
            BufferedWriter output = new BufferedWriter(new FileWriter(textResult))){
            int text;
            while ((text = input.read()) != -1) {
                output.write(text);
            }
        } catch (IOException e) {
            e.getSuppressed();
        }
    }
}

