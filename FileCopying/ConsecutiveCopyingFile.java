package FileCopying;
/*
* класс где последовательно запускаются два потока
* и пользователю выводиться время за которое выполниться эти два потока
 */
public class ConsecutiveCopyingFile extends Thread {
    public static void main(String args[]) throws InterruptedException {
        final String SERIAL_EXECUTION_TIME_OF_THE_COPY = "Время выполнения последовательного копирования файла: %d";

        Thread threadOne = new Thread() {
            @Override
            public void run() {
                CopyFile.fileCopy();
            }
        };
        Thread threadTwo = new Thread() {
            @Override
            public void run() {
                CopyFile.fileCopy();
            }
        };
        final long before = System.currentTimeMillis();
        threadOne.start();
        threadOne.join();
        threadTwo.start();
        threadTwo.join();
        final long after = System.currentTimeMillis();
        System.out.printf(SERIAL_EXECUTION_TIME_OF_THE_COPY, (after - before));
    }
}

