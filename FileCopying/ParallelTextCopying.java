package FileCopying;
/*
 * класс где паралельно запускаются два потока
 * и пользователю выводиться время за которое выполниться эти два потока
 */
public class ParallelTextCopying extends Thread {
    public static void main(String args[]) throws InterruptedException {
        final String TIME_PARALLEL_EXECUTION_COPY = "Время выполнения параллельного копирования файла: %d";

        Thread threadOne = new Thread() {
            @Override
            public void run() {
                CopyFile.fileCopy();
            }
        };
        Thread threadTwo = new Thread() {
            @Override
            public void run() {
                CopyFile.fileCopy();
            }
        };
        final long before = System.currentTimeMillis();
        threadOne.start();
        threadTwo.start();
        threadOne.join();
        threadTwo.join();
        final long after = System.currentTimeMillis();
        System.out.printf(TIME_PARALLEL_EXECUTION_COPY, (after - before));
    }
}
